<?php

use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;

return array(
    'datetime' => [
        /**
         * Datetime configuration used on client side.
         */
        'client' => [
            /**
             * Datetime configuration used to render dates.
             */
            'get' => [
                'timezone' => date_default_timezone_get(),
                /**
                 * Datetime format can be string|string[].
                 * @see DefaultDateTimeFactory get_datetime_format configuration format
                 * If list provided, first is considered as datetime format base,
                 * @see DefaultDateTimeFactory note about datetime format base.
                 *
                 */
                'format' => [
                    'long' => 'Y-m-d H:i:s',
                    'short' => 'Y-m-d'
                ]
            ],

            /**
             * Datetime configuration used to hydrate dates.
             */
            'set' => [
                'timezone' => date_default_timezone_get(),
                /**
                 * Datetime format can be string|string[].
                 * @see DefaultDateTimeFactory set_datetime_format configuration format
                 * If list provided, first is considered as datetime format base,
                 * @see DefaultDateTimeFactory note about datetime format base.
                 *
                 */
                'format' => [
                    'long' => 'Y-m-d H:i:s',
                    'short' => 'Y-m-d'
                ]
            ]
        ],

        /**
         * Datetime configuration used on application side,
         * to manage dates.
         */
        'application' => [
            'timezone' => 'UTC'
        ],

        /**
         * Datetime configuration used on storage side,
         * to save and load dates.
         */
        'storage' => [
            'timezone' => 'UTC',
            'format' => 'Y-m-d H:i:s'
        ]
    ]
);