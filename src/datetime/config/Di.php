<?php

use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;



return array(
    // Entity datetime services
    // ******************************************************************************

    'entity_datetime_factory' => [
        'source' => DefaultDateTimeFactory::class,
        'argument' => [
            ['type' => 'array', 'value' => [
                ['key' => 'entity_timezone_name', 'type' => 'config', 'value' => 'datetime/application/timezone'],
                ['key' => 'get_timezone_name', 'type' => 'config', 'value' => 'datetime/client/get/timezone'],
                ['key' => 'get_datetime_format', 'type' => 'config', 'value' => 'datetime/client/get/format'],
                ['key' => 'set_timezone_name', 'type' => 'config', 'value' => 'datetime/client/set/timezone'],
                ['key' => 'set_datetime_format', 'type' => 'config', 'value' => 'datetime/client/set/format'],
                ['key' => 'save_timezone_name', 'type' => 'config', 'value' => 'datetime/storage/timezone'],
                ['key' => 'save_datetime_format', 'type' => 'config', 'value' => 'datetime/storage/format']
            ]]
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    DateTimeFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'entity_datetime_factory'],
        'option' => [
            'shared' => true,
        ]
    ]
);